import json
import csv
from os import listdir
from datetime import datetime
import pytz

def utc_to_time(naive, timezone="US/Central"):
    return naive.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(timezone))

FIELDS_STREAM = ['artistName', 'trackName', 'msPlayed', 'endTime','hour','minsPlayed']
path = 'spotifyPlays/'
files = [history for history in listdir(path) if 'StreamingH' in history]

i = 0
for file in files:
    with open(path+file,'r') as stream:
        streamingHist = json.loads(stream.read())
        for song in streamingHist:
            streamDate = datetime.strptime(song['endTime'], "%Y-%m-%d %H:%M")
            realStreamDate = utc_to_time(streamDate)
            song['endTime'] = realStreamDate.strftime("%Y-%m-%d %H:%M")

            hour = realStreamDate.hour
            song['hour'] = hour
            song['minsPlayed'] = round(song['msPlayed']/1000/60,2)
            print(song['minsPlayed'])
    
    action_file = 'a'
    if i == 0: action_file = 'w'
    with open('test.csv',action_file, encoding='utf-8') as testW:
        writer = csv.DictWriter(testW,fieldnames=FIELDS_STREAM)
        if i == 0:
            i +=1
            writer.writeheader()
        for song in streamingHist:
            writer.writerow(song)
